This app is pre-setup with an admin account. The initial credentials are:
  
**Email**: admin@example.com<br/>
**Password**: changeme<br/>

<sso>
Cloudron users are given the default role of administrator. Set `AUTH_CLOUDRON_DEFAULT_ROLE_ID` to a custom role id in `/app/data/env.sh` to override this.
</sso>


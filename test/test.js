#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    let browser, app, cloudronName;

    const adminEmail = 'admin@example.com',
        adminPassword = 'changeme';

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(email, password) {
        await browser.get('https://' + app.fqdn + '/admin/login');
        await waitForElement(By.xpath('//input[@type="email"]'));
        await browser.findElement(By.xpath('//input[@type="email"]')).sendKeys(email);
        await browser.findElement(By.xpath('//input[@type="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await browser.sleep(6000);
        await browser.wait(until.elementLocated(By.xpath('//a[contains(@href, "/admin/users/")]')), TEST_TIMEOUT);
    }

    async function loginOIDC(username, password, alreadyAuthenticated = false) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/admin/login`);
        await browser.sleep(2000);

        await waitForElement(By.xpath(`//a[contains(., "Log In with ${cloudronName}")]`));
        await browser.findElement(By.xpath(`//a[contains(., "Log In with ${cloudronName}")]`)).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(2000);
        }

        await waitForElement(By.xpath('//a[contains(@href, "/admin/users/")]'));
    }

    async function logout() {
        await browser.manage().getCookies();
        await browser.sleep(3000);
        await browser.get('https://' + app.fqdn + '/admin/logout');
        await browser.sleep(3000);
    }

    async function createCollection() {
        await browser.get('https://' + app.fqdn + '/admin/settings/data-model/+');
        if (app.manifest.version === '1.29.0') {
            await waitForElement(By.xpath('//h1[text()="Creating New Collection"]'));
        } else {
            await waitForElement(By.xpath('//div[text()="Creating New Collection"]'));
        }
        await browser.findElement(By.xpath('//input[contains(@placeholder, "unique table name")]')).sendKeys('goodiebag');
        await browser.findElement(By.xpath('//i[@data-icon="arrow_forward"]')).click();
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//i[@data-icon="check"]')).click();
        if (app.manifest.version === '1.29.0') {
            await waitForElement(By.xpath('//h1[text()="Goodiebag"]'));
        } else {
            await waitForElement(By.xpath('//div[text()="Goodiebag"]'));
        }
    }

    async function checkCollection() {
        await browser.get('https://' + app.fqdn + '/admin/settings/data-model/goodiebag');
        if (app.manifest.version === '1.29.0') {
            await waitForElement(By.xpath('//h1[text()="Goodiebag"]'));
        } else {
            await waitForElement(By.xpath('//div[text()="Goodiebag"]'));
        }
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');

        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no SSO
    it('install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can Admin login', login.bind(null, adminEmail, adminPassword));
    it('create collection', createCollection);
    it('check collection', checkCollection);
    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // SSO
    it('install app (SSO)', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can Admin login', login.bind(null, adminEmail, adminPassword));
    it('create collection', createCollection);
    it('check collection', checkCollection);
    it('can logout', logout);

    it('can login OIDC', loginOIDC.bind(null, username, password, false));
    it('check collection', checkCollection);
    it('can logout', logout);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id); });

    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('check collection', checkCollection);
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('check collection', checkCollection);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can Admin login', login.bind(null, adminEmail, adminPassword));
    it('check collection', checkCollection);
    it('can logout', logout);

    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('check collection', checkCollection);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app from App Store', function () { execSync(`cloudron install --appstore-id io.directus9.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can Admin login', login.bind(null, adminEmail, adminPassword));
    it('create collection', createCollection);
    it('can logout', logout);

    it('can update', async function () { execSync('cloudron update --app ' + app.id, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can Admin login', login.bind(null, adminEmail, adminPassword));
    it('check collection', checkCollection);
    it('can logout', logout);

    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('check collection', checkCollection);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});

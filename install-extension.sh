#!/usr/bin/bash

set -eu

extension_name=$1

if [[ $# -ne 1 ]]; then
    echo "Usage: install-extension.sh <extension-name>"
    exit 1
fi

if [[ -d "/app/data/extensions/${extension_name}" ]]; then
    echo "extension already exists at /app/data/extensions/${extension_name} . Please delete it and try again"
    exit 1
fi

tmpdir=$(gosu cloudron:cloudron mktemp -d)
trap "rm -rf ${tmpdir}" EXIT

# when installing with -g , npm will install it as a standalone module with a proper package.json
# without -g, it ends up as a dependancy in package.json . the --prefix is where to install it
gosu cloudron:cloudron npm install -g --prefix "${tmpdir}" "${extension_name}"
mv "${tmpdir}/lib/node_modules/${extension_name}" /app/data/extensions

echo -e "\n\nExtension installed at /app/data/extensions/${extension_name}. Restart Directus to enable the extension"


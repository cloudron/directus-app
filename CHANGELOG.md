[0.1.0]
* Initial version

[1.0.0]
* Update to Directus 9.0.1

[1.1.0]
* Update Directus to 9.1.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.1.0)

[1.1.1]
* Update Directus to 9.1.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.1.1)
* #10051 fix notifications button hover color on dark theme (@azrikahar)
* #10057 Fix mysql duplicates (@Nitwel)
* #10050 fix roles aggregation query to fit all db vendors (@azrikahar)
* #10048 Set isEditorDirty flag to track edits in wysiwyg html editor (@licitdev)
* #10040 Fix data model folders edit dialog (@azrikahar)
* #3456 Add cloudron as installation method (@gramakri)

[1.2.0]
* Update Directus to 9.2.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.2.0)
* #10279 Add items.read filter hook (@Oreilles)
* #9322 Implement AUTH_DISABLE_DEFAULT config option (@dorianim)
* #10098 Add dark mode to docs (@u12206050)
* #8010 Support 'selectionMode' on tabular and cards (@joselcvarela)

[1.2.1]
* Update Directus to 9.2.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.2.1)
* #10346 Use variables for default colors in boolean interface/display (@rijkvanzanten)
* #10340 replace logo with rounded version (@benhaynes)
* #10331 Update input-rich-text-html.vue (@AndreyKindin)

[1.2.2]
* Update Directus to 9.2.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.2.2)

[1.3.0]
* Update Directus to 9.3.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.3.0)
* Update to base image v3.2.0

[1.4.0]
* Update Directus to 9.4.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.4.0)

[1.4.1]
* Update Directus to 9.4.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.4.2)

[1.4.2]
* Update Directus to 9.4.3
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.4.3)

[1.5.0]
* Update Directus to 9.5.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.5.0)

[1.5.1]
* Update Directus to 9.5.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.5.1)

[1.5.2]
* Update Directus to 9.5.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.5.2)

[1.6.0]
* Enable postgis extension

[1.7.0]
* Update Directus to 9.6.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.6.0)

[1.8.0]
* Update Directus to 9.7.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.7.0)

[1.8.1]
* Update Directus to 9.7.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.7.1)
* #12170 Add App Translation Strings in Settings (by @azrikahar)
* #12324 Add shortcut from data model to collection content (by @Tummerhore)
* #12310 Save last accessed collection in Content Module (by @azrikahar)
* #12276 Fix field preview background color (by @azrikahar)

[1.9.0]
* Update Directus to 9.8.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.8.0)
* #12503 [SDK] Add further request options to items functions (by @tschortsch)
* #12488 Add functions support to the app + add count function (by @rijkvanzanten)
* #12363 Add field-level validation (by @rijkvanzanten)
* #12488 Add functions support to the app + add count function (by @rijkvanzanten)
* #12363 Add field-level validation (by @rijkvanzanten)
* #8196 Added default locale before login (by @christianrr)

[1.10.0]
* Update Directus to 9.9.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.9.0)
* #12732 Adds x-directus-cache response header with HIT value (by @keesvanbemmel)
* #12687 Add support for translatable bookmark names (by @rijkvanzanten)
* #12082 Relational Interfaces Rework rabbithole (by @Nitwel)
* #11620 sparkles adding possibility to update another field value from a custom interface (by @AntoineBarroux)
* #12575 Add import file method to SDK (by @azrikahar)

[1.10.1]
* Update Directus to 9.9.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.9.1)
* #12947 Add extensions to fix mp4 file icons (by @azrikahar)
* #12936 Only watch for wysiwyg changes on first interaction (by @rijkvanzanten)
* #12929 Show minutes by default in Calendar month layout (by @azrikahar)
* #12869 Input translated string improvements (by @azrikahar)
* #12865 Improve app loading background (by @LasseRosenow)
* #12822 Tweak default sizing of roles, enable resize (by @rijkvanzanten)
* #11946 Emoji Picker & Comment improvements (by @Nitwel)

[1.11.0]
* Update Directus to 9.10.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.10.0)
* #13099 Add local export capability (by @rijkvanzanten)
* #13004 Allow configuring the HSTS header (by @rijkvanzanten)
* #12084 Add support to sort on a nested value (by @nodeworks)

[1.12.0]
* Update Directus to 9.11.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.11.0)
* #11871 GraphQL count aggregation for all fields and * (by @jeengbe)
* #11737 Add support to insensitive case operators (by @bernatvadell)
* #11737 Add support to insensitive case operators (by @bernatvadell)

[1.12.1]
* Update Directus to 9.11.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.11.1)
* #13518 Don't require default connection params when using non default type (by @rijkvanzanten)
* #13514 Default default-src to "self" in CSP header (by @rijkvanzanten)
* #13492 Allow floats in number validation (by @rijkvanzanten)
* #13274 Environment variable with `_FILE` suffix containing invalid path throws error on start (by @br41nslug)
* #13511 Don't show regex on permissions configuration (by @rijkvanzanten)
* #13501 Add empty state for Translation Strings page (by @azrikahar)
* #13462 Improve dashboard saving experience (by @nickrum)

[1.13.0]
* Update Directus to 9.12.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.12.0)
* #12522 Add Data Flows to Directus (by @Nitwel)
* #12522 Add Data Flows to Directus (by @Nitwel)
* #11186 Docs: Auto-generated "Link preview" thumbnails for all pages (by @loteoo)

[1.13.1]
* Update Directus to 9.12.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.12.1)
* #13724 Only render first 10000 items on calendar layout view (by @rijkvanzanten)
* #13723 Don't crash on misconfigured scope trigger (by @rijkvanzanten)
* #13713 Fix flows editing existing operations (by @rijkvanzanten)
* #13635 Add Locale labels to groups (by @azrikahar)
* #13719 Fix installer missing package (by @rijkvanzanten)
* #13694 Fix export offset (by @licitdev)
* #13709 Fix endpoint extensions being registered under wrong route (by @nickrum)

[1.13.2]
* Update Directus to 9.12.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.12.2)
* #13797 Use latest vue version when scaffolding extension packages (by @nickrum)
* #13796 Use JobQueue when reloading extensions (by @nickrum)
* #12044 Enable activity tracking at password reset (by @ybelenko)
* #13777 Allow selection of image transformation in the WYSIWYG interface (by @licitdev)
* #13772 Add configurable page-size to files/o2m/m2m/m2a interfaces (by @rijkvanzanten)
* #12804 Take into account the format option when displaying dates relative. (by @u12206050)

[1.14.0]
* Update Directus to 9.13.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.13.0)
* #14096 Insights 2.0 (by @rijkvanzanten)
* #13924 API: Add env var to opt-out mailer setup verification (by @joselcvarela)
* #13907 Allow setting a custom filename in the /assets endpoint for SEO (by @rijkvanzanten)
* #13871 Add optional cache max value size limit configuration (by @rijkvanzanten)
* #14096 Insights 2.0 (by @rijkvanzanten)
* #13867 Add search in v-select (by @azrikahar)
* #13576 Allow setting default language for translations interface or choosing user's language (by @u12206050)
* #7805 Add tfa enforce flow (by @Nitwel)

[1.15.0]
* Update Directus to 9.14.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.14.0)
* #14203 Emitter emits event in the meta (by @licitdev)
* #14303 Prevents selecting relational fields as sort field (by @br41nslug)
* #14300 Fixes data missing after sort in m2a interface (by @br41nslug)
* #14252 Remove auto-open groups in v-select (by @azrikahar)
* #14251 Fix crash on logout due to te missing (by @addisonElliott)
* #14241 broken image display for null assets (by @br41nslug)
* #14234 Don't force reset scroll on hash changes (by @rijkvanzanten)
* #14233 Fix conditions crash when custom options component is used (by @rijkvanzanten)
* #14231 Fix vertical alignment of render-template in tabular (by @rijkvanzanten)
* #14227 Persist existing global variable values on save (by @azrikahar)
* #14216 Only emit value if updated for wysiwyg interface (by @licitdev)
* #14195 Fix rendering of social icons (by @licitdev)
* #14188 Performance improvements for groups within v-form (by @br41nslug)
* #13970 Fix Save As Copy with edited relational fields (by @younky-yang)

[1.15.1]
* Update Directus to 9.14.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.14.1)
* #14326 Fix SSO login buttons missing in login page (by @younky-yang)
* #14321 Show field data type & interface on hover for system collections' fields (by @azrikahar)

[1.15.2]
* Update Directus to 9.14.3
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.14.3)
* #14541 Select multiple dropdown preview threshold (by @rijkvanzanten)
* #14412 Enable spellcheck on wysiwyg and markdown interfaces (by @licitdev)
* #14390 Fix strict relative dates showing "incorrect" (by @u12206050)
* #14021 Add raw editor toggle for using variables in flows operations (by @azrikahar)
* #10592 Optimize media loading across app (by @jaycammarano)
* #10488 Adding editor to image component (by @juancarlosjr97)

[1.15.3]
* Update Directus to 9.14.5
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.14.5)
* #14576 Revert to for image editor (by @licitdev)
* #14569 System fields not properly filtered when editing images (by @br41nslug)

[1.16.0]
* Update Directus to 9.15.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.15.0)
* #14833 Improve cache performance by compressing records (by @joselcvarela)
* #14786 Support custom aspect ratios in image editor (by @azrikahar)
* #14410 Add support for operation extensions to the Extensions SDK (by @nickrum)

[1.16.1]
* Update Directus to 9.15.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.15.1)
* #14878 Add styling to links in field note (by @azrikahar)

[1.17.0]
* Update Directus to 9.16.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.16.0)

[1.17.1]
* Update Directus to 9.16.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.16.1)

[1.18.0]
* Update Directus to 9.17.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.17.0)
* #10551 Implement query hook (by @sebj54)
* #15366 Reset value when no changes (by @Nitwel)
* #15273 Add origin to accountability (by @licitdev)
* #15267 Revert list style for O2M and M2M interfaces (by @azrikahar)
* #15243 Allow for displaying lists in render template (by @Nitwel)
* #15218 Add missing translations (by @Nitwel)
* #15200 Automatic range on insights graph (by @HuldaCZ)
* #15128 Make all options from upload menu directly accessible (by @Tummerhore)
* #15094 Add Components Package (by @Nitwel)
* #14700 Refresh current item on flow run & prompt when there's unsaved changes (by @azrikahar)

[1.18.1]
* Update Directus to 9.17.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.17.1)
* #15384 Adding SendGrid email transport (by @naskio)
* #14605 fix divider not showing/ showing when not needed (drawer-item) (by @NigmaX)
* #15403 Disable foreign check outside the trx in SQLite (by @licitdev)
* #15396 #15395 fix: date-fns date format for tr-TR translations (by @kadiresen)
* #15386 Merge with existing item when validating in drawer-item (by @licitdev)
* #15385 Set limit as -1 for local exports when field is cleared (by @licitdev)

[1.18.2]
* Update Directus to 9.17.4
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.17.4)
* #15413 Fix drawer item empty form for o2m/treeview (by @azrikahar)
* #15433 Merge with M2M junction value when validating in drawer-item (by @licitdev)
* #15438 Prevent v-highlight infinite loop (by @azrikahar)

[1.19.0]
* Update Directus to 9.18.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.18.0)
* #15452 Relational Selection Panel for Insights Variable (by @br41nslug)
* #15570 Only pass singleton prop in CollectionOrItem component when necessary (by @azrikahar)
* #15548 Use the improved get method (by @Nitwel)
* #15514 Allow user to send a WYSIWYG email body (by @raflymln)
* #15514 Allow user to send a WYSIWYG email body (by @raflymln)

[1.19.1]
* Update Directus to 9.18.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.18.1)
* #15611 Add support for <collection>.items.query filter hook (by @mtgerb)
* #15693 Graphql: Fix required PK on create / update (by @joselcvarela)
* #15162 Implemented an option to filter M2A fields using graphql. (by @maximka1221)

[1.20.0]
* Update Directus to 9.19.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.19.0)

[1.20.1]
* Update Directus to 9.19.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.19.1)

[1.20.2]
* Update Directus to 9.19.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.19.2)

[1.21.0]
* Update Directus to 9.20.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.20.1)

[1.21.1]
* Update Directus to 9.20.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.20.2)

[1.21.2]
* Update Directus to 9.20.3
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.20.3)

[1.21.3]
* Update Directus to 9.20.4
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.20.4)

[1.22.0]
* Enable LDAP integration

[1.23.0]
* Update Directus to 9.21.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.21.0)
* #16453 Optimize number of times cache is being cleared in ItemsService updateBatch (by @azrikahar)
* #16436 Allow admin to update Directus User provider and external_identifier (by @azrikahar)
* #16099 Returns the nodemailer promise (by @keesvanbemmel)
* #16436 Allow admin to update Directus User provider and external_identifier (by @azrikahar)
* #16379 Fix sortField selection (by @Nitwel)
* #16375 Add editsGuard to drawerItem (by @Nitwel)

[1.23.1]
* Update Directus to 9.21.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.21.1)
* #16641 Fixup: Re-add entrypoint type definition files in @directus/shared (by @paescuj)
* #16606 Re-add entrypoint type definition files in @directus/shared (by @paescuj)
* #16570 fix missing collection after sorting in M2A (by @azrikahar)
* #16518 Fix .module-nav-resize-handle layout (by @d1rOn)
* #16562 Propagate mutation options for schema apply (by @licitdev)

[1.24.0]
* Update Directus to 9.22.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.21.0)
* #15672 Add support for a package extension bundle type (by @nickrum)
* #16453 Optimize number of times cache is being cleared in ItemsService updateBatch (by @azrikahar)
* #16436 Allow admin to update Directus User provider and external_identifier (by @azrikahar)
* #16099 Returns the nodemailer promise (by @keesvanbemmel)

[1.24.1]
* Update Directus to 9.22.4
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.22.4)
* #17101 offset should be 0 not 1 by default (by @freekrai)
* #17098 Fix running npm init directus-extension (by @Nitwel)

[1.24.2]
* Set memory limit to 512Mb to ensure reliable app startup

[1.25.0]
* Update Directus to 9.23.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.23.1)
* #17416 Make sort fields hidden by default (by @Nitwel)
* #17199 Update locale name for zh-TW (by @azrikahar)
* #16773 Flows improvements (by @Nitwel)
* #15269 Improve preview of relational columns in tabular layout (by @Nitwel)

[1.25.1]
* Update Directus to 9.23.4
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.23.2)
* #17914 Fix cookie redaction in logs (by @br41nslug)
* #17908 Fix auth expires value larger than 32-bit integer for GraphQL (by @azrikahar)
* #17903 Fixed `_nempty` operator (by @br41nslug)
* sponge Optimizations
* #17904 TS Config Modernization Program Part 3 of many (by @rijkvanzanten)
* warning #17858 Simplify / improve release flow (by @rijkvanzanten)
* Fixes push to npm

[1.26.0]
* Update Directus to 9.24.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.24.0)
* #17603 Add optional confirmation dialog with input fields to Manual Trigger in Flows (by @rijkvanzanten)
* #18011 Add active state to file interface (by @azrikahar)
* #17958 Fix collection name on deselect in m2a relation (by @Nitwel)
* #17954 Export types in shared (by @Nitwel)
* #18032 add missing .toString() for password reset accept URL object (by @azrikahar)
* #18029 Fix cache skipping when `PUBLIC_URL` is relative (by @licitdev)
* #17976 Only check for undefined and null (by @Nitwel)
* #17972 Do not override metadata (by @Nitwel)
* #17922 Add missing fields to GraphQL server info (by @azrikahar)
* #17957 Don't move files when folder can't be deleted (by @Nitwel)
* #17956 Properly apply query param to url (by @Nitwel)
* #17953 Only hide v-select options (by @Nitwel)
* #17891 Fix Save and Stay to refresh revisions detail for roles and webhooks (by @azrikahar)
* #17848 Don't set edits at start in List Panel (by @Nitwel)
* #17842 App: Fix saving required relational fields (by @joselcvarela)
* #17746 Fix sorting in m2m table (by @Nitwel)

[1.27.0]
* Update Directus to 9.25.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.25.0)
* **warning** Potential Breaking Change(s): In this one, we've updated the API codebase from CJS to ESM. From our testing, this should be backwards compatible with API-extensions created with the extensions-sdk. However, due to the finicky nature of this change in Node, please do make sure to test your extensions before updating to this version. If you're running into ESM-CJS import issues, try renaming your extension output to .cjs instead of .js or bundle it to native ESM.
* #18131 Add register and update hooks for oauth2 and openid drivers (by @nicam)
* #18012 Autoconvert assets if browser supports it (by @Nitwel)
* #17303 Add support for AVIF image format (by @knulpi)
* #18167 Only show message on just request/exec flow operations (by @ConnorSimply)
* #18166 fix: Use Cloudinary explicit endpoint (by @jbmolle)
* #18154 Load Bundle Extension Types for local development (by @that1matt)
* #18147 Resend user invites (by @david-zacharias)

[1.27.1]
* Update Directus to 9.25.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.25.2)
* #18256 Fall back to "other" group if interface has none (by @nickrum)
* #18242 Use filled star for required asterisks & rating display (by @azrikahar)
* #18226 Add comment about sorting of available languages & add Spanish (Mexico) (by @paescuj)
* #18200 Add loader to logs-siderbar-detail (by @hanneskuettner)
* #18160 Add select all option for export (by @jaads)
* #15733 Add possibility of sorting items in tabular view of o2m interface (by @d1rOn)
* #18238 Remove cache flushing on startup (by @rijkvanzanten)
* #18229 Vary on accept when auto converting assets (#18228) (by @bicouy0)
* #18266 Fix wrong drawer opens when editing fields in data model (by @Nitwel)
* #18196 Fix m2a/m2a/o2m sorting (by @hanneskuettner)
* #18194 Prevent duplicate emit from CodeMirror editors (by @hanneskuettner)
* #18193 Rename system-field[-tree] collection prop to collectionName (by @hanneskuettner)
* #18185 Fix Material Symbols in timycme & mapbox (by @hanneskuettner)
* #18027 Add autoKey to interface config (by @hanneskuettner)
* #18257 Fix extension reloading (by @nickrum)
* #18235 Don't fail on ?format=auto without accept header (by @rijkvanzanten)
* #18188 Fix .default is not a function errors (by @azrikahar)
* #18202 Use import.meta.url instead of `__dirname` (by @hanneskuettner)
* #18180 Adjust paths in extensions-sdk & publish composables types (by @paescuj)
* #18187 Fix npm init directus-extension (by @azrikahar)

[1.28.0]
* Update Directus to 9.26.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v9.26.0)
* #15829 [Feat] Flow: Add Mail Templates (by @JonathanSchndr)
* #15829 [Feat] Flow: Add Mail Templates (by @JonathanSchndr)
* #18303 Schema cache auto-purge (by @br41nslug)
* #18284 Only show tooltips when sidebar is not open (by @azrikahar)
* #16373 Improve Permissions in relational Interfaces (by @Nitwel)
* #18275 Fix the ownership of the workdir in the docker image (by @paescuj)

[1.29.0]
* Update Directus to 10.0.0
* Directus is adopting BSL 1.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.0.0)

[1.29.1]
* Rename `/app/data/env` to `/app/data/env.sh`

[1.30.0]
* Update Directus to 10.1.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.1.0)
* Added Cache option so that caching can be disabled for GET requests to Flows webhook trigger (#18277 by @licitdev)
* Added Kanban layout (#18516 by @azrikahar)
* Added bar chart, line chart, pie chart, and meter panels (#18522 by @azrikahar)
* Added folder navigation to the file(s) / image selector interfaces, making it easier to browse and select assets (#18320 by @paescuj)
* Added block editor interface (#18525 by @azrikahar)
* Added Cache option so that caching can be disabled for GET requests to Flows webhook trigger (#18277 by @licitdev)
* Added support for the multipart/form-data content type in the /schema/apply endpoint (#18321 by @jaads)
* Added Pressure-based rate limiter (#17873 by @rijkvanzanten)
* Added support for the multipart/form-data content type in the /schema/apply endpoint (#18321 by @jaads)
* Added Pressure-based rate limiter (#17873 by @rijkvanzanten)
* Added support for building API extensions to ESM format and default to ESM for new extensions (#18351 by @nickrum)
* Added support for building API extensions to ESM format and default to ESM for new extensions (#18351 by @nickrum)

[1.30.1]
* Update Directus to 10.1.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.1.1)
* Add new toggle to collapse nested collections in the datamodel settings (#18547 by @that1matt)
* Added a maximum for query limit (#17309 by @br41nslug)
* Fixed invalid field types in grouping for kanban layout (#18602 by @licitdev)
* Fixed render issue in file item route (#18581 by @paescuj)
* Updated Vue to 3.3 (#18588 by @paescuj)
* Added missing translation in presentation-notice interface (#18591 by @paescuj)
* Fixed drag & drop upload to the root folder in the file library (#18601 by @paescuj)
* Added a maximum for query limit (#17309 by @br41nslug)
* Fixed api endpoint for insights list panel (#18631 by @br41nslug)
* Fixed issue with interface forms not rendering when interface itself has no default value (#18610 by @paescuj)
* Fixed default values for interfaces & displays (#18611 by @paescuj)
* Fixed users-invite component's incorrect null check (#18583 by @licitdev)
* Allow batchMode and batchActive to be used in interface extensions (#18624 by @timio23)
* Fixed field detail logic to ensure options for list & map interfaces are displayed again (#18603 by @paescuj)
* Fixed deselecting options for kanban layout (#18600 by @licitdev)

[1.31.0]
* Update Directus to 10.2.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.2.0)

[1.32.0]
* Update Directus to 10.3.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.3.0)

[1.33.0]
* Update Directus to 10.4.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.4.0)
* Fixed archive status in sidebar when clicking on same collection again (#18995 by @AshishBarvaliya)
* Implemented sorting in PostgreSQL driver (#18954 by @jaads)
* Added limit and offset support for postgres driver (#18894 by @jaads)
* Implemented sort conversion in SQL middleware (#18938 by @jaads)
* Fixed display template with nested data for items in calendar layout (#19011 by @AshishBarvaliya)
* Updated list of collections for Event Hooks in Flows to not include folders (#19007 by @AshishBarvaliya)

[1.33.1]
* Update Directus to 10.4.3
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.4.3)
* Added Collection and Item Options to Flows Notification Operation (#19033 by @ConnorSimply)
* Fixed User Display not being an option for m2o and string (#19093 by @ConnorSimply)
* fixed editorjs icon styling (#19118 by @ConnorSimply)
* Fixed issue that would prevent Vite from hot reloading local app extensions during development (#19075 by @luochuanyuewu)
* Fixed Flow Status not saving in editor (#19092 by @ConnorSimply)
* Fixed errors processing for starts_with and ends_with filter variations (#19021 by @br41nslug)
* Fixed issue that would cause the validation rules editor to show a field selection dropdown where no fields are (#19124 by @rijkvanzanten)
* available
* Resolved issue that would allow you to save the parent Flow with a keyboard shortcut while editing an operation (#19108 by @rijkvanzanten)
* Fixed issue that would prevent changes in the raw edit dialog from showing up in the template input in flows and (#19126 by @rijkvanzanten)
* insights
* Fixed limit values in export drawer when a query limit is in place (#18731 by @paescuj)
* Fixed cleanup of unused isBatch variable to solve missing file preview in file item page (#19114 by @joselcvarela)

[1.34.0]
* Update Directus to 10.5.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.5.0)
* First release of the new SDK rocket New modular architecture, real-time support, TypeScript optimized, and tons of other goodies! (#18987 by @br41nslug)
* Add support for Supabase Storage for files (#19135 by @matt-rolley)
* Enabled zoom view on readonly image interfaces (#19221 by @AshishBarvaliya)
* Fixed issue that would cause number formatting to be wrong for certain locales in Insights (#19137 by @AshishBarvaliya)
* Add support for Supabase Storage for files (#19135 by @matt-rolley)
* Added support for setting deep in the useItems composable (#18713 by @u12206050)
* Added tsconfig for Vue & CJS (#19197 by @paescuj)

[1.34.1]
* Update Directus to 10.5.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.5.1)
*     Fixed issue with trailing slashes in URL util preventing app extension from being loaded (#19252 by @paescuj)

[1.34.2]
* Update Directus to 10.5.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.5.2)
* Enabled usage of Home and End keys in input fields (#19273 by @gitstart)
* Updated TinyMCE to fix issue with readonly mode (#19280 by @paescuj)
* Resolved browser freeze in field conditions by delaying the retrieving of field context (#19271 by @paescuj)
* Enhanced logic to fetch user data on user detail page (#19257 by @paescuj)

[1.34.3]
* Update Directus to 10.5.3
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.5.3)
* Fixed showing of detailed and improved simple Rating-Display for the Card-Layout (#19365 by @DanielBiegler)
* Fixed displaying of thumbnail in O2M/M2O/M2A interfaces (#19395 by @paescuj)
* Introduced option to control lazy loading attribute for images in WYSIWYG interface (#19353 by @Nitwel)
* Added file preview to the drawer if the relation is `directus_files` (#19374 by @DanielBiegler)
* Fixed hiding of modals after failing to delete item (#19383 by @DanielBiegler)

[1.35.0]
* Update Directus to 10.6.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.6.0)

[1.35.1]
* Update Directus to 10.6.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.6.2)
* Allow for cross-origin cookies to be sent with fetch, fixed parameter order for refresh request, introduced withOptions helper and renamed asSearch to withSearch for consistency (#19354 by @br41nslug)
* Improved the notifications tab by showing message contents, adding "Archive All" option and adding filtering and pagination (#19503 by @Nitwel)
* Added the target prop for external links on the v-button component (#19561 by @Dominic-Marcelino)
* Reset auto_increment sequence for PostgreSQL when PK is set manually (#19328 by @jaads)
* Included the response object in thrown errors and added the request object in the onResponse hook (#19539 by @br41nslug)

[1.35.2]
* Update Directus to 10.6.3
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.6.3)
* Fixed the format of the stored field conditions value (#19799)
* Introduced option to configure the display of repeater fields (#19703 by @Nitwel)
* Fixed replacing an asset when importing a file via URL (#19788 by @DanielBiegler)
* Removed padding in v-highlight to fix various display issues (#19740 by @paescuj)
* Enhanced display of status & role in user popup (#19790 by @paescuj)
* Fixed the format of the stored field conditions value (#19799 by @paescuj)
* Fixed rendering the user-popover for users without a role (#19774 by @0x2aff)
* Made search in data model page case-insensitive (#19751 by @paescuj)
* Made highlighted text more visible in dark mode (#19748 by @paescuj)
* Fixed loading JSON and YAML configuration files (#19745 by @jbmolle)
* Fixed updating filename and file-extension after replacing asset (#19771 by @0x2aff)
* Fixed replacing an asset when importing a file via URL (#19788 by @DanielBiegler)
* Fixed issue where user create/update wouldn't validate emails in the same way user authentication does (#19794 by @rijkvanzanten)
* Add error message for email validation failure (#19794 by @rijkvanzanten)
* Added an extra JSON Mime-Type check to the SDK response parsing (#19786 by @br41nslug)
* Fixed WebSocket subscription output typing in the SDK (#19791 by @br41nslug)
* Removed invalid type constraint on the SDK client request function (#19789 by @br41nslug)
* Removed default values for fetch credentials in the SDK (#19749 by @br41nslug)
* Added explicit usage of mode in login and refresh calls in the SDK (#19766 by @br41nslug)
* Enabled bundling of the package (#19714 by @paescuj)

[1.35.3]
* Update Directus to 10.6.4
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.6.4)
* Added auto-detection of delimiter for CSV imports, allowing the use of other delimiters than , (#19739 by @u12206050)
* Fixed automatic sequence reset for capitalized table names in PostgreSQL (#19825 by @jaads)
* Added cacheClear method to the UtilsService (#19990 by @nodeworks)
* Add new read-asset functions (#20041 by @br41nslug)
* Added support for literal field types in the SDK (#19792 by @br41nslug)
* Added more modifiers to the data abstraction and m2o join (#19146 by @jaads)
* Added more modifiers to the data abstraction and m2o join (#19146 by @jaads)
* Added more modifiers to the data abstraction and m2o join (#19146 by @jaads)
* Show folder navigation in file interfaces with configured "Root Folder" (#19518 by @Nitwel)
* Made API errors available through @directus/errors (#20054 by @Boegie19)

[1.36.0]
* Update Directus to 10.7.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.7.0)

[1.36.1]
* Update Directus to 10.7.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.7.1)
* Added explicit "Project Default" option to user appearance setting (#20194 by @rijkvanzanten)
* Updated Bug Report and Feature Request Links in Settings to Use Templates (#20197 by @ConnorSimply)
* Added theming fields to app minimal and recommended permissions (#20198 by @azrikahar)
* Improved error handling on login-screen when trying to log in without credentials (#20151 by @DanielBiegler)
* Fixed an issue where forms with detail/accordion fields starting closed would freeze the browser once opened (#20185 by @paescuj)

[1.36.2]
* Update Directus to 10.7.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.7.2)
* Added support for configurable navigation dividers (#20277 by @rijkvanzanten)
* Added support for configurable borders (#20226 by @rijkvanzanten)
* Improved the theme rules overrides interface by rendering the global rules under a new "globals" key (#20280 by @rijkvanzanten)
* Added support for configurable navigation dividers (#20277 by @rijkvanzanten)
* Added support for configurable borders (#20226 by @rijkvanzanten)
* Improve rolling deployment experience (#20308 by @rijkvanzanten)
* Fixed incorrect input pattern in the app (#20263 by @br41nslug)
* Fix activity log button not matching theme rules for sidebar detail (#20275 by @rijkvanzanten)
* Improve rolling deployment experience (#20308 by @rijkvanzanten)
* Fixed importing files with predefined ids (#20301 by @br41nslug)
* Fixed legacy endpoint route name (#20231 by @nickrum)
* Fixed an issue where removing a local extension with autoload true resulted in a crash. (#20259 by @ComfortablyCoding)
* Trimmed sort array in query (#20254 by @licitdev)
* Fixed query validation for SEARCH requests (#20253 by @br41nslug)

[1.37.0]
* Update Directus to 10.8.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.8.0)
* Added theme-selector interface to settings and users (#20413 by @rijkvanzanten)
* Return theme information from server project info endpoint (#20324 by @rijkvanzanten)
* Added a new login page default art (#20309 by @benhaynes)
* Added support for overriding global backgrounds (#20311 by @rijkvanzanten)
* Added support for themes as an extension type (#20423 by @rijkvanzanten)
* Added support for configuring font weights, popover menus, input height and spacing, and fix various issues in theming (#20426 by @rijkvanzanten)

[1.37.1]
* Update Directus to 10.8.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.8.1)
* Fixed form paddings in default dark theme, enhanced theme selector interface (#20455 by @paescuj)
* Fixed OAS generation to be based on permissions (#20386 by @br41nslug)
* Use the hostname as the OAS URL if no PUBLIC_URL has been specified (#20460 by @paescuj)
* Ensured the paths and schemas in the generated OAS are based on current user (#20462 by @paescuj)
* Ensured the paths and schemas in the generated OAS are based on current user (#20462 by @paescuj)
* Fixed form paddings in default dark theme, enhanced theme selector interface (#20455 by @paescuj)

[1.37.2]
* Update Directus to 10.8.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.8.2)
* Cleaned-up unused translation strings (#20494 by @paescuj)
* Fixed default values for system-theme-overrides interface (#20464 by @azrikahar)
* Fixed full permissions & minimal app permissions (#20505 by @azrikahar)
* Fixed typo for user last_access field in OAS (#20465 by @azrikahar)

[1.37.3]
* Update Directus to 10.8.3
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.8.3)

[1.38.0]
* Add script `/app/pkg/install-extension.sh` to install extensions

[1.38.1]
* Move Directus from LDAP to OpenID Connect
* **It may take up to 10min to be able to re-login after the update**

[1.39.0]
* Update Directus to 10.9.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.9.0)
* Enabled theme override permissions to be set on a per role basis (#20899 by @ComfortablyCoding)
* Added focal point support for images (#20768 by @DanielBiegler)
* Added a hash display (#20883 by @ComfortablyCoding)
* Added enable/disable support for bundle type extensions (#20940 by @ComfortablyCoding)
* Prevented loading of sidebar details (revisions, comments, shares etc) until they are opened (#20848 by @ComfortablyCoding)
* Added an icon and icon color option for the select dropdown interface choices, added an icon and icon color option for label display choices and added default colors for the status field display (#20782 by @ComfortablyCoding)

[1.39.1]
* Update Directus to 10.9.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.9.1)
* Added support for using video files in the public background setting. (#21291 by @rijkvanzanten)
* Fixed the permission check in the Data Studio for permissions rules containing fields which the current user has no access to, via new dedicated permission check API endpoint (#21152 by @paescuj)
* Fixed manual flow dialog showing above select item drawer (#21361 by @ComfortablyCoding)
* Fixed the permission check in the Data Studio for permissions rules containing fields which the current user has no access to, via new dedicated permission check API endpoint (#21152 by @paescuj)
* Fixed collection folder dialog showing above translation drawer (#21368 by @paescuj)
* Fixed document title formatting (#21400 by @br41nslug)
* Fixed an issue where form fields could crash when filled out with Chrome's autofill functionality (#21384 by @AshishBarvaliya)
* Fixed the issue with events not being triggered anymore on data imports (#21406 by @paescuj)
* Resolved error indicating inability to remove last admin user during user updates via roles (#21343 by @paescuj)
* Fixed public branding assets permission issue (#21360 by @br41nslug)
* Fixed the permission check in the Data Studio for permissions rules containing fields which the current user has no access to, via new dedicated permission check API endpoint (#21152 by @paescuj)
* Fixed the treatment of environment variables when set to an empty string (#21371 by @paescuj)

[1.39.2]
* Update Directus to 10.9.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.9.2)
* Filter system collections based on the system property (#21197 by @br41nslug)
* Fixed various issues in the presentation-links component (#21338 by @DanielBiegler)
* Cleaned-up and fixed IP validation used for external requests (#21370 by @paescuj)
* Updated dependencies (#21428 by @paescuj)

[1.39.3]
* Update Directus to 10.9.3
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.9.3)
* Fixed the field state of conditions in field settings and enabled the undo action (#21498 by @paescuj)
* Ensured overlapping popovers from block editor are rendered above other interfaces' elements (#21522 by @paescuj)
* Fixed image select dialog showing above file drawer for markdown interface (#21520 by @paescuj)
* Fixed an issue that prevented logging in when emails contained unicode characters (#21454 by @DanielBiegler)
* Fixed no results text for dashboard search and hide search if there are no dashboards (#21499 by @paescuj)

[1.40.0]
* Update Directus to 10.10.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.10.0)
* This release contains the extensions marketplace and big improvements to content versioning.
* Integrated the Content Version in the browser URL / history to enable browser navigation and page refresh (#21624 by @paescuj)
* Added a new set of extensions registry endpoints (#21674 by @paescuj)
* If you're using extensions in type-folders, please be aware of breaking changes

[1.40.1]
* Update Directus to 10.10.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.10.1)
* Fixed issues with the marketplace migration script for MySQL/MariaDB, as well as for multiple bundle extensions having (#21721 by @paescuj)
* same sub-extension names
* Added support for the EMAIL_TEMPLATES_PATH to allow email templates to exist in any given location (#21729 by @rijkvanzanten)
* Fixed an issue that would cause the host field of the package extension manifest to be the sdk version rather than (#21730 by @rijkvanzanten)
* directus version
* Fixed an issue that would cause extensions loading to fail when non-extension folders were included in the extensions folder (#21718 by @br41nslug)

[1.40.2]
* Update Directus to 10.10.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.10.2)
* Prevented leaving the edit mode of a Flow via save shortcut while editing the trigger or adding a new operation (#21736 by @fanmingfei)
* Fixed the height of list items which do have thumbnails (#21756 by @formfcw)
* Prevented an error which could occur when sandboxed extensions are reloaded (for example via extensions auto reload) (#21781 by @paescuj)
* Fixed loading of Sandbox Operation Extensions (#21751 by @paescuj)
* Enabled usage of extensions auto reloading in dev mode (NODE_ENV set to development) (#21780 by @paescuj)
* Fixed an issue that would cause the extensions folder sync from third party sources to fail if the sync request came from a secondary process (#21747 by @rijkvanzanten)
* Ensured errors thrown in Sandbox SDK functions are reported correctly (#21749 by @paescuj)

[1.40.3]
* Update Directus to 10.10.3
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.10.3)

[1.40.4]
* Update Directus to 10.10.4
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.10.4)
* Fixed an issue that could cause the graphql system endpoint to use the wrong schema (#21821 by @rijkvanzanten)
* Corrected the type of the filter query parameter in OpenAPI specs (#21802 by @paescuj)

[1.40.5]
* Fix marketplace extension installation

[1.40.6]
* Update Directus to 10.10.5
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.10.5)

[1.40.7]
* Update Directus to 10.10.7
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.10.7)
* Fixed an issue that would cause the project to hang in an unavailable state if an unexpected external error caused the (#22230 by @joselcvarela)
* schema retrieval to fail in a high load environment
* Reverted cache-control header change to prevent cache inconsistencies in-app (#22235 by @rijkvanzanten)
* Fixed an issue that could cause errors happening during schema retrieval to exit the process rather than return a 500 (#22231 by @joselcvarela)
* Updated the cache clear endpoint to allow for clearing cache, system, or both (#22234 by @joselcvarela)

[1.41.0]
* Update Directus to 10.11.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.11.0)
* Fixed API queries with the search parameter to return no results if the query is not applicable to any fields (#22342)
* Added API and UI for public user registration (#22125 by @DanielBiegler)
* Added API and UI for public user registration (#22125 by @DanielBiegler)
* Added GraphQL singleton helper type for the SDK (#22270 by @magiudev)
* Extended isDirectusError guard to return specific error type when code for built-in error is provided (#22346 by @paescuj)

[1.41.1]
* Update Directus to 10.11.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.11.1)
* Fixed the cards layout if used with files that fail loading (#22530 by @hanneskuettner)
* Reduced the number of session token refreshes if the token is still fresh (#22464 by @licitdev)
* Fixed the failing auto-creating of default languages for translation fields when using an existing, custom language collection. (#22409 by @hanneskuettner)
* Added missing translations for focal point coordinate form fields (#22458 by @DanielBiegler)
* Updated dependencies (#22424 by @dependabot)
* Fixed display template not showing newly added fields under collection field settings (#22430 by @hanneskuettner)
* Added reload button when the app errors out (#22462 by @licitdev)
* Ensured items are correctly loaded when switching the view in the calendar layout (#22517 by @paescuj)
* Fixed missing field selection dropdown for functions in filter interface (#22426 by @hanneskuettner)

[1.41.2]
* Update Directus to 10.11.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.11.2)
* Added ability to customize the URL for email verification when registering users (#22565 by @DanielBiegler)

[1.42.0]
* Update Directus to 10.12.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.12.0)
* Implemented configurable limits for active users with Admin / App / API access (#22479 by @licitdev)
* Implemented batch update functions in the SDK (#22564 by @br41nslug)

[1.42.1]
* Update Directus to 10.12.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.12.1)

[1.43.0]
* Update Directus to 10.13.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.13.0)

[1.43.1]
* Update Directus to 10.13.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.13.1)

[1.43.2]
* Update Directus to 10.13.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v10.13.2)

[2.0.0]
* Update Directus to 11.0.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v11.0.0)
* **Important:** Directus v11 contains a brand new permissions system that's based on policies. We've provided a migration, so the upgrade path is the same as with other releases. This is a big release, which changes the paradigm on how permissions are attached and executed.

[2.0.1]
* Update Directus to 11.0.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v11.0.1)
* Added transaction retry mechanism for SQLite if a SQLITE_BUSY errors occurs (#23243 by @joggienl)
* Added support for listening on unix sockets via a new UNIX_SOCKET_PATH variable (#23150 by @McSundae)
* Added support to provide a cast prefix to config variables which are read from a file using the _FILE suffix (#22164 by @joselcvarela)
* Removed update delay in the block editor interface (#23115 by @SP12893678)
* Fixed list structure in draggable lists (#23116 by @SP12893678)
* Fixed an issue that would cause the translations display not to use the correct language if the user relied on "system (#23240 by @danilobuerger)
* Improved policies migration to handle a missing foreign key on directus_permissions.role more gracefully (#23251 by @hanneskuettner)
* Ensured the migrations are properly executed when bootstrapping MySQL (#23233 by @paescuj)
* Fixed drop foreign key if has different constraint name on permissions policies migrations (#23253 by @joselcvarela)
* Fixed the init CLI command for MySQL to use the new mysql2 package (#23231 by @paescuj)

[2.0.2]
* Update Directus to 11.0.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v11.0.2)

[2.1.0]
* Update Directus to 11.1.0
* [Full changelog](https://github.com/directus/directus/releases/tag/v11.1.0)

[2.1.1]
* Update Directus to 11.1.1
* [Full changelog](https://github.com/directus/directus/releases/tag/v11.1.1)

[2.1.2]
* Update Directus to 11.1.2
* [Full changelog](https://github.com/directus/directus/releases/tag/v11.1.2)

[2.2.0]
* Update Directus to 11.2.0
* [Full Changelog](https://github.com/directus/directus/releases/tag/v11.2.0)
* Added TUS support to the Supabase storage driver (#23958 by @ComfortablyCoding)
* Added TUS support to the Azure storage driver (#23904 by @ComfortablyCoding)
* Added TUS support to the Cloudinary storage driver (#23943 by @ComfortablyCoding)
* Added TUS support to the GCS storage driver (#23897 by @ComfortablyCoding)
* 

[2.2.1]
* Update Directus to 11.2.1
* [Full Changelog](https://github.com/directus/directus/releases/tag/v11.2.1)

[2.2.2]
* Update directus to 11.2.2
* [Full Changelog](https://github.com/directus/directus/releases/tag/v11.2.2)
* Fixed enforcing `chunkSize` when TUS is not enabled and updated the default `TUS_CHUNK_SIZE` to `8mb` ([#&#8203;24002](https://github.com/directus/directus/pull/24002) by [@&#8203;ComfortablyCoding](https://github.com/ComfortablyCoding))

[2.3.0]
* Update directus to 11.3.1
* [Full Changelog](https://github.com/directus/directus/releases/tag/v11.3.1)

[2.3.1]
* Update directus to 11.3.2
* [Full Changelog](https://github.com/directus/directus/releases/tag/v11.3.2)
* **[@&#8203;directus/app](https://github.com/directus/app)**
* **[@&#8203;directus/api](https://github.com/directus/api)**
* `@directus/app@13.3.6`
* `@directus/api@23.2.2`

[2.3.2]
* checklist added to CloudronManifest
* CLOUDRON_OIDC_PROVIDER_NAME implemented

[2.3.3]
* Update directus to 11.3.3
* [Full Changelog](https://github.com/directus/directus/releases/tag/v11.3.3)

[2.3.4]
* Update directus to 11.3.4
* [Full Changelog](https://github.com/directus/directus/releases/tag/v11.3.4)

[2.3.5]
* Update directus to 11.3.5
* [Full Changelog](https://github.com/directus/directus/releases/tag/v11.3.5)

[2.4.0]
* Update directus to 11.4.0
* [Full Changelog](https://github.com/directus/directus/releases/tag/v11.4.0)

[2.4.1]
* Update directus to 11.4.1
* [Full Changelog](https://github.com/directus/directus/releases/tag/v11.4.1)
* **[@&#8203;directus/app](https://github.com/directus/app)**
* **[@&#8203;directus/app](https://github.com/directus/app)**
* **[@&#8203;directus/api](https://github.com/directus/api)**
* **[@&#8203;directus/sdk](https://github.com/directus/sdk)**
* **[@&#8203;directus/themes](https://github.com/directus/themes)**
* Removed invalid operation trigger API from SDK and docs ([#&#8203;24461](https://github.com/directus/directus/pull/24461) by [@&#8203;licitdev](https://github.com/licitdev))
* Patched blackbox tests for v11 ([#&#8203;24281](https://github.com/directus/directus/pull/24281) by [@&#8203;licitdev](https://github.com/licitdev))

[2.5.0]
* Update directus to 11.5.0
* [Full Changelog](https://github.com/directus/directus/releases/tag/v11.5.0)

[2.5.1]
* Update directus to 11.5.1
* [Full Changelog](https://github.com/directus/directus/releases/tag/v11.5.1)


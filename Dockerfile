FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=directus/directus versioning=semver extractVersion=^v(?<version>.+)$
ARG DIRECTUS_VERSION=11.5.1

# this can also be installed from source via pnpm (https://docs.directus.io/contributing/running-locally.html#running-locally)
RUN npm init -y && \
    npm install directus@${DIRECTUS_VERSION}

RUN ln -s /tmp/directus/cache /app/code/.cache

COPY install-extension.sh start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]


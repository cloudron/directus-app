#!/bin/bash

set -eu

mkdir -p /app/data/files /tmp/directus/cache /tmp/directus/temp /run/directus /run/cloudron.npm /app/data/extensions

# ensure that data directory is owned by 'cloudron' user
chown -R cloudron:cloudron /app/data /run/cloudron.npm /tmp/directus /run/directus

# https://github.com/directus/directus/blob/main/api/example.env
export PORT=8055
export PUBLIC_URL="${CLOUDRON_APP_ORIGIN}"
export LOG_LEVEL="info"
export LOG_STYLE="pretty"

export TEMP_PATH="/tmp/directus/temp"
       
export DB_CLIENT="pg" 
export DB_HOST="$CLOUDRON_POSTGRESQL_HOST"
export DB_PORT="$CLOUDRON_POSTGRESQL_PORT"
export DB_DATABASE="$CLOUDRON_POSTGRESQL_DATABASE"
export DB_USER="$CLOUDRON_POSTGRESQL_USERNAME"
export DB_PASSWORD="$CLOUDRON_POSTGRESQL_PASSWORD"
       
export RATE_LIMITER_ENABLED=true
export RATE_LIMITER_POINTS=100
export RATE_LIMITER_DURATION=1
export RATE_LIMITER_STORE="redis"
export RATE_LIMITER_EXEC_EVENLY=false
export RATE_LIMITER_BLOCK_DURATION=0
export RATE_LIMITER_KEY_PREFIX="rlflx"       

export CACHE_ENABLED=true
export CACHE_TTL="10m"
export CACHE_NAMESPACE="directus-cache"
export CACHE_STORE="redis"
export CACHE_AUTO_PURGE="true"
       
# it does not like to have the username passed in the URL
export REDIS="redis://:${CLOUDRON_REDIS_PASSWORD}@${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}"
       
export STORAGE_LOCATIONS="local"
export STORAGE_LOCAL_PUBLIC_URL="/files"
export STORAGE_LOCAL_DRIVER="local"
export STORAGE_LOCAL_ROOT="/app/data/files"

export ACCESS_TOKEN_TTL="15m"
export REFRESH_TOKEN_TTL="7d"
export REFRESH_TOKEN_COOKIE_SECURE="false"
export REFRESH_TOKEN_COOKIE_SAME_SITE="lax"
export REFRESH_TOKEN_COOKIE_NAME="directus_refresh_token"

export CORS_ENABLED="true"
export CORS_ORIGIN="*"
export CORS_METHODS="GET,POST,PATCH,DELETE" 
export CORS_ALLOWED_HEADERS="Content-Type,Authorization"
export CORS_EXPOSED_HEADERS2="Content-Range"
export CORS_CREDENTIALS="true"
export CORS_MAX_AGE=18000

export EXTENSIONS_PATH="/app/data/extensions"

export EMAIL_FROM="$CLOUDRON_MAIL_FROM" 
export EMAIL_TRANSPORT="smtp" 

export EMAIL_SMTP_POOL=false
export EMAIL_SMTP_HOST="$CLOUDRON_MAIL_SMTP_SERVER"
export EMAIL_SMTP_PORT="$CLOUDRON_MAIL_SMTP_PORT"
export EMAIL_SMTP_SECURE=false
export EMAIL_SMTP_USER="$CLOUDRON_MAIL_SMTP_USERNAME"
export EMAIL_SMTP_PASSWORD="$CLOUDRON_MAIL_SMTP_PASSWORD"

echo "==> Enabling postgis extension"
PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "CREATE EXTENSION IF NOT EXISTS postgis;"

if [[ ! -f /app/data/.initialized ]]; then
    echo "==> New install, initializing"

    # we cannot use .local for email, because it wants a real domain
    ADMIN_EMAIL="admin@example.com" ADMIN_PASSWORD="changeme" /usr/local/bin/gosu cloudron:cloudron npx directus bootstrap  
    touch /app/data/.initialized
fi

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "==> Enabling OIDC"
    export AUTH_PROVIDERS="cloudron"
    export AUTH_DISABLE_DEFAULT="false" # allow local users

    export AUTH_CLOUDRON_DRIVER="openid"
    export AUTH_CLOUDRON_CLIENT_ID="${CLOUDRON_OIDC_CLIENT_ID}"
    export AUTH_CLOUDRON_CLIENT_SECRET="${CLOUDRON_OIDC_CLIENT_SECRET}"
    export AUTH_CLOUDRON_SCOPE="openid profile email"
    export AUTH_CLOUDRON_ISSUER_URL="${CLOUDRON_OIDC_ISSUER}/.well-known/openid-configuration"

    export AUTH_CLOUDRON_IDENTIFIER_KEY="sub"
    export AUTH_CLOUDRON_ALLOW_PUBLIC_REGISTRATION=true
    export AUTH_CLOUDRON_REQUIRE_VERIFIED_EMAIL=false
    default_role=$(PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -XAwt -c "select id from directus_roles WHERE name='Administrator';")
    export AUTH_CLOUDRON_DEFAULT_ROLE_ID="${default_role}"
    export AUTH_CLOUDRON_ICON="account_circle"
    export AUTH_CLOUDRON_LABEL="${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}"
fi


if [[ ! -f /app/data/env.sh ]]; then
    sudo -u cloudron cat > /app/data/env.sh <<EOF
export KEY="$(uuid -v4)"
export SECRET="$(pwgen -1 32)"
EOF
fi

cp /app/data/env.sh /run/directus/env.sh
[[ -f /app/data/env ]] && cat /app/data/env >> /run/directus/env.sh

source /run/directus/env.sh

echo "==> DB Migration"
/usr/local/bin/gosu cloudron:cloudron npx directus database migrate:latest

echo "==> Starting Directus"
exec /usr/local/bin/gosu cloudron:cloudron npx directus start

